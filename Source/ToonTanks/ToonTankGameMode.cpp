// All rights reserved, Innocent Sign Inn


#include "ToonTankGameMode.h"
#include "Tank.h"
#include "Tower.h"
#include "TankPlayerController.h"
#include "Kismet/GameplayStatics.h"

void AToonTankGameMode::BeginPlay()
{
	Super::BeginPlay();
	HandleGameStart();
	TargetTowerCount = GetTargetTowerCount();
}

void AToonTankGameMode::ActorDied(AActor* DeadActor)
{
	if (DeadActor == PlayerTank)
	{
		PlayerTank->HandleDestruction();
		if (TanksPlayerController)
		{
			TanksPlayerController->SetPlayerEnabledState(false);
			GameOver(false);
		}
	}
	else if (ATower* destroyedTower = Cast<ATower>(DeadActor))
	{
		destroyedTower->HandleDestruction();

		UE_LOG(LogTemp, Warning, TEXT("Tower destroyed!!!, %d"), TargetTowerCount);

		if (--TargetTowerCount <= 0)
		{
			GameOver(true);
		}
	}
}

void AToonTankGameMode::HandleGameStart()
{
	PlayerTank = Cast<ATank>(UGameplayStatics::GetPlayerPawn(this, 0));
	TanksPlayerController = Cast<ATankPlayerController>(UGameplayStatics::GetPlayerController(this, 0));

	StartGame();

	if (TanksPlayerController)
	{
		TanksPlayerController->SetPlayerEnabledState(false);
		FTimerHandle PlayerStartHandle;
		FTimerDelegate PlayerEnableTimerDelegate = FTimerDelegate::CreateUObject(
			TanksPlayerController, &ATankPlayerController::SetPlayerEnabledState, true);
		GetWorldTimerManager().SetTimer(PlayerStartHandle, PlayerEnableTimerDelegate, StartDelay, false);
	}
}

int32 AToonTankGameMode::GetTargetTowerCount()
{
	TArray<AActor*> AllTowers;
	UGameplayStatics::GetAllActorsOfClass(this, TowerClass, AllTowers);
	return AllTowers.Num();
}
