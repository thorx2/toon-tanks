// All rights reserved, Innocent Sign Inn

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

UCLASS()
class TOONTANKS_API AProjectile : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* BaseMeshComp;

	UPROPERTY(VisibleAnywhere, Category = "Movement")
	class UProjectileMovementComponent* ProjectileMoveComp;

	UPROPERTY(EditAnywhere)
	float Damage;

	UPROPERTY(EditAnywhere, Category = "Combat VFX")
	class UParticleSystem* ExplosionEffect;

	UPROPERTY(VisibleDefaultsOnly)
	class UParticleSystemComponent* TrailEffect;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* uComp, AActor* otherActor, UPrimitiveComponent* otherComp, FVector normalImpulse,
	           const FHitResult& hit);

	UPROPERTY(EditDefaultsOnly, Category = "Combat SFX")
	class USoundBase* LaunchSound;

	UPROPERTY(EditDefaultsOnly, Category = "Combat SFX")
	class USoundBase* HitSound;

	UPROPERTY(EditDefaultsOnly, Category = "Combat VFX")
	TSubclassOf<class UCameraShakeBase> HitCameraShakeClass;
};
