// All rights reserved, Innocent Sign Inn

#pragma once

#include "CoreMinimal.h"
#include "BasePawn.h"
#include "Tank.generated.h"

UCLASS()
class TOONTANKS_API ATank : public ABasePawn
{
	GENERATED_BODY()

public:
	ATank();

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tank Parameters", meta = (AllowPrivateAccess = "true"))
	float TankSpeed = 5.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tank Parameters", meta = (AllowPrivateAccess = "true"))
	float TankRotationSpeed = 5.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* SpringArmComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* CameraComp;
#pragma region Input_References
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input Mappings", meta = (AllowPrivateAccess = "true"))
	class UInputMappingContext* InputMapping;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input Mappings", meta = (AllowPrivateAccess = "true"))
	class UInputAction* MoveForwardAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input Mappings", meta = (AllowPrivateAccess = "true"))
	class UInputAction* FireGunAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input Mappings", meta = (AllowPrivateAccess = "true"))
	class UInputAction* TurnTurretAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input Mappings", meta = (AllowPrivateAccess = "true"))
	class UInputAction* TurnTankAction;
#pragma endregion
#pragma region Input_Callbacks
	void MoveActionCallbackFunc(const struct FInputActionInstance& Instance);

	void FireActionCallbackFunc(const struct FInputActionInstance& Instance);

	void TurnTankActionCallbackFunc(const struct FInputActionInstance& Instance);

	void TurnTurretActionCallbackFunc(const struct FInputActionInstance& Instance);
#pragma endregion 
	APlayerController* TankPlayerController;

	virtual void FireProjectiles() override;

	bool bAlive = true;

public:
	virtual void HandleDestruction() override;

	APlayerController* GetPlayerController() const;

	bool IsAlive() const;
};
