// All rights reserved, Innocent Sign Inn

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ToonTankGameMode.generated.h"

/**
 * 
 */
UCLASS()
class TOONTANKS_API AToonTankGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	void ActorDied(AActor* DeadActor);

protected:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent)
	void StartGame();

	UFUNCTION(BlueprintImplementableEvent)
	void GameOver(bool bWonGame);

private:
	class ATank* PlayerTank;

	class ATankPlayerController* TanksPlayerController;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category= "Configuration", meta=(AllowPrivateAccess))
	float StartDelay = 3.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Metadata Class", meta = (AllowPrivateAccess = "true"))
	TSubclassOf<class ATower> TowerClass;

	void HandleGameStart();
	
	int32 TargetTowerCount = 0;

	int32 GetTargetTowerCount();
};
