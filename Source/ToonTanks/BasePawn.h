// All rights reserved, Innocent Sign Inn

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "BasePawn.generated.h"

UCLASS()
class TOONTANKS_API ABasePawn : public APawn
{
	GENERATED_BODY()

public:
	ABasePawn();

protected:

	void RotateTurret(const FVector& lookAtTarget, const float deltaTime);

	virtual void FireProjectiles();

	FVector GetProjectileSpawnPoint();

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	class UCapsuleComponent* CapsuleComp;
	// Forward declaration with "class", reason to skip including the header in anther header

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* BaseMeshComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* TurretMeshComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	USceneComponent* SpawnPoint;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Turret Parameters", meta = (AllowPrivateAccess = "true"))
	float TurretRotationSpeed = 5.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Combat", meta = (AllowPrivateAccess = "true"))
	TSubclassOf<class AProjectile> ProjectileClass;

	UPROPERTY(EditDefaultsOnly, Category = "Combat VFX")
	class UParticleSystem* DeathExplosion;

	UPROPERTY(EditDefaultsOnly, Category = "Combat SFX")
	class USoundBase* DeathSound;

	UPROPERTY(EditDefaultsOnly, Category = "Combat VFX")
	TSubclassOf<class UCameraShakeBase> DeathCameraShakeClass;
	
public:
	virtual void HandleDestruction();
};
