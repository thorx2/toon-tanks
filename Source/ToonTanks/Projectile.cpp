// All rights reserved, Innocent Sign Inn


#include "Projectile.h"
#include "Kismet/GameplayStatics.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Particles/ParticleSystemComponent.h"
// Sets default values
AProjectile::AProjectile()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// Built in function to create a sub object component
	BaseMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));

	// Setting the capsule as the root component
	RootComponent = BaseMeshComp;

	ProjectileMoveComp = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMoveComp"));
	ProjectileMoveComp->MaxSpeed = 1300;
	ProjectileMoveComp->InitialSpeed = 50;

	TrailEffect = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("TrailRenderer"));
	TrailEffect->SetupAttachment(BaseMeshComp);
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	BaseMeshComp->OnComponentHit.AddDynamic(this, &AProjectile::OnHit);
	UGameplayStatics::PlaySoundAtLocation(this, LaunchSound, GetActorLocation());
}

void AProjectile::OnHit(UPrimitiveComponent* uComp, AActor* otherActor, UPrimitiveComponent* otherComp,
                        FVector normalImpulse, const FHitResult& hit)
{
	if (GetOwner() == nullptr)
	{
		Destroy();
		return;
	}
	if (otherActor && otherActor != this && otherActor != GetOwner())
	{
		UGameplayStatics::ApplyDamage(otherActor, Damage, GetOwner()->GetInstigatorController(), this,
		                              UDamageType::StaticClass());

		if (ExplosionEffect)
		{
			UGameplayStatics::PlaySoundAtLocation(this, HitSound, GetActorLocation());
			UGameplayStatics::SpawnEmitterAtLocation(this, ExplosionEffect, GetActorLocation(), GetActorRotation());
		}

		if (HitCameraShakeClass)
		{
			GetWorld()->GetFirstPlayerController()->ClientStartCameraShake(HitCameraShakeClass);
		}
	}
	Destroy();
}
