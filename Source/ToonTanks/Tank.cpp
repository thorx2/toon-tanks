// All rights reserved, Innocent Sign Inn

#include "Tank.h"
#include "InputMappingContext.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/LocalPlayer.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"

ATank::ATank() : ABasePawn()
{
	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("Camera Boom"));

	SpringArmComp->SetupAttachment(RootComponent);

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));

	CameraComp->SetupAttachment(SpringArmComp);
}

// Called when the game starts or when spawned
void ATank::BeginPlay()
{
	Super::BeginPlay();

	ULocalPlayer* localPlayer = GetWorld()->GetFirstLocalPlayerFromController();

	if (UEnhancedInputLocalPlayerSubsystem* InputSystem = localPlayer->GetSubsystem<
		UEnhancedInputLocalPlayerSubsystem>())
	{
		if (InputMapping != NULL)
		{
			InputSystem->AddMappingContext(InputMapping, 5000);
		}
	}

	TankPlayerController = Cast<APlayerController>(GetController());

	TankPlayerController->SetShowMouseCursor(true);
}

// Called every frame
void ATank::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (TankPlayerController)
	{
		FHitResult HitResult;
		bool hasHit = TankPlayerController->GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, false, HitResult);
		if (hasHit)
		{
			RotateTurret(HitResult.ImpactPoint, DeltaTime);
		}
	}
}

void ATank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	UEnhancedInputComponent* Input = Cast<UEnhancedInputComponent>(PlayerInputComponent);
	// You can bind to any of the trigger events here by changing the "ETriggerEvent" enum value
	Input->BindAction(MoveForwardAction, ETriggerEvent::Triggered, this, &ATank::MoveActionCallbackFunc);

	Input->BindAction(FireGunAction, ETriggerEvent::Triggered, this, &ATank::FireActionCallbackFunc);

	Input->BindAction(TurnTankAction, ETriggerEvent::Triggered, this, &ATank::TurnTankActionCallbackFunc);

	Input->BindAction(TurnTurretAction, ETriggerEvent::Triggered, this, &ATank::TurnTurretActionCallbackFunc);
}

void ATank::FireProjectiles()
{
	Super::FireProjectiles();
}

void ATank::HandleDestruction()
{
	Super::HandleDestruction();
	bAlive = false;
	SetActorHiddenInGame(true);
	SetActorTickEnabled(false);
}

APlayerController* ATank::GetPlayerController() const
{
	return TankPlayerController;
}

bool ATank::IsAlive() const
{
	return bAlive;
}

void ATank::MoveActionCallbackFunc(const FInputActionInstance& Instance)
{
	float forwardInput = Instance.GetValue().Get<float>();

	FVector forwardDeltaLocation(0.0f);

	forwardDeltaLocation.X = TankSpeed * forwardInput * UGameplayStatics::GetWorldDeltaSeconds(this);

	AddActorLocalOffset(forwardDeltaLocation, true);
}

void ATank::FireActionCallbackFunc(const FInputActionInstance& Instance)
{
	bool FloatValue = Instance.GetValue().Get<bool>();
	FireProjectiles();
}

void ATank::TurnTankActionCallbackFunc(const FInputActionInstance& Instance)
{
	float FloatValue = Instance.GetValue().Get<float>();
	FRotator rotationDelta(0.0f);
	rotationDelta.Yaw = TankRotationSpeed * FloatValue * UGameplayStatics::GetWorldDeltaSeconds(this);
	AddActorLocalRotation(rotationDelta, true);
}

void ATank::TurnTurretActionCallbackFunc(const FInputActionInstance& Instance)
{
	float FloatValue = Instance.GetValue().Get<float>();
	UE_LOG(LogTemp, Warning, TEXT("Turn turret!!! %f"), FloatValue);
}
