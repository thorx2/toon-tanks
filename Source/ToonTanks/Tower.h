// All rights reserved, Innocent Sign Inn

#pragma once

#include "CoreMinimal.h"
#include "BasePawn.h"
#include "Tower.generated.h"

/**
 *
 */
UCLASS()
class TOONTANKS_API ATower : public ABasePawn
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	bool ClearSightForTarget();

private:
	class ATank* TargetTank;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tower Parameters", meta = (AllowPrivateAccess = "true"))
	float FireRange = 300.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tower Parameters", meta = (AllowPrivateAccess = "true"))
	float FireRate = 2.0f;

	FTimerHandle FireRateTimerHandle;

	void CheckFireCondition();

public:
	virtual void HandleDestruction() override;
};
