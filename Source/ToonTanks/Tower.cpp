// All rights reserved, Innocent Sign Inn

#include "Tower.h"
#include "Tank.h"
#include "Engine/TimerHandle.h"
#include "Kismet/GameplayStatics.h"

void ATower::BeginPlay()
{
	Super::BeginPlay();

	TargetTank = Cast<ATank>(UGameplayStatics::GetPlayerPawn(this, 0));

	GetWorldTimerManager().SetTimer(FireRateTimerHandle, this, &ATower::CheckFireCondition, FireRate, true);
}

void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (TargetTank && TargetTank->IsAlive())
	{
		float distanceToTank = FVector::Dist(GetActorLocation(), TargetTank->GetActorLocation());

		if (distanceToTank < FireRange)
		{
			RotateTurret(TargetTank->GetActorLocation(), DeltaTime);
			if (ClearSightForTarget())
			{
				if (!GetWorldTimerManager().IsTimerActive(FireRateTimerHandle))
				{
					GetWorldTimerManager().UnPauseTimer(FireRateTimerHandle);
				}
			}
			else if (GetWorldTimerManager().IsTimerActive(FireRateTimerHandle))
			{
				GetWorldTimerManager().PauseTimer(FireRateTimerHandle);
			}
		}
		else if (GetWorldTimerManager().IsTimerActive(FireRateTimerHandle))
		{
			GetWorldTimerManager().PauseTimer(FireRateTimerHandle);
		}
	}
}

void ATower::CheckFireCondition()
{
	FireProjectiles();
}

void ATower::HandleDestruction()
{
	Super::HandleDestruction();
	Destroy();
}

bool ATower::ClearSightForTarget()
{
	FHitResult targetFound;
	if (GetWorld()->LineTraceSingleByChannel(targetFound, GetProjectileSpawnPoint(), TargetTank->GetActorLocation(),
	                                         ECollisionChannel::ECC_Visibility))
	{
		if (targetFound.GetActor()->GetName().Contains("Tank"))
		{
			return true;
		}
	}
	return false;
}
