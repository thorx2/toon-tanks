// All rights reserved, Innocent Sign Inn

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class TOONTANKS_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	void SetPlayerEnabledState(bool bEnabled);
};
