// All rights reserved, Innocent Sign Inn


#include "TankPlayerController.h"

void ATankPlayerController::SetPlayerEnabledState(bool bEnabled)
{
	if (bEnabled)
	{
		GetPawn()->EnableInput(this);
	}
	else
	{
		GetPawn()->DisableInput(this);
	}

	bShowMouseCursor = bEnabled;
}
