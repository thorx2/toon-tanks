// Fill out your copyright notice in the Description page of Project Settings.

#include "BasePawn.h"
#include "Components/CapsuleComponent.h"
#include "Projectile.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABasePawn::ABasePawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	// Built in function to create a sub object component
	CapsuleComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleCollider"));
	// Setting the capsule as the root component
	RootComponent = CapsuleComp;

	BaseMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));

	BaseMeshComp->SetupAttachment(CapsuleComp);

	TurretMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TurretMesh"));

	TurretMeshComp->SetupAttachment(BaseMeshComp);

	SpawnPoint = CreateDefaultSubobject<USceneComponent>(TEXT("Projectile Spawn Point"));

	SpawnPoint->SetupAttachment(TurretMeshComp);
}

void ABasePawn::RotateTurret(const FVector& lookAtTarget, const float deltaTime)
{
	FVector toTarget = lookAtTarget - TurretMeshComp->GetComponentLocation();

	FRotator lookAtRot = FRotator(0, toTarget.Rotation().Yaw, 0);

	TurretMeshComp->SetWorldRotation(FMath::RInterpTo(TurretMeshComp->GetComponentRotation(), lookAtRot, deltaTime,
	                                                  TurretRotationSpeed));
}


void ABasePawn::FireProjectiles()
{
	FVector spawnLoc = SpawnPoint->GetComponentLocation();
	AProjectile* projectile = GetWorld()->SpawnActor<AProjectile>(ProjectileClass, spawnLoc,
	                                                              SpawnPoint->GetComponentRotation());
	projectile->SetOwner(GetOwner());
}

FVector ABasePawn::GetProjectileSpawnPoint()
{
	return SpawnPoint->GetComponentLocation();
}


void ABasePawn::HandleDestruction()
{
	UGameplayStatics::SpawnEmitterAtLocation(this, DeathExplosion, GetActorLocation(), GetActorRotation());
	UGameplayStatics::PlaySoundAtLocation(this, DeathSound, GetActorLocation());

	if (DeathCameraShakeClass)
	{
		GetWorld()->GetFirstPlayerController()->ClientStartCameraShake(DeathCameraShakeClass);
	}
}
